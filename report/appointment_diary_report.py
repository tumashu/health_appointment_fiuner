# -*- coding: utf-8 -*-

from trytond.pool import Pool
from trytond.report import Report

from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
from trytond.transaction import Transaction


__all__ = ['AppointmentDiaryReport']

class AppointmentDiaryReport(Report):
    'Appointment Report'
    __name__ = 'gnuhealth.appointment.appointment_report'

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        Appointments = pool.get('gnuhealth.appointment')
        Healthprofessional =pool.get('gnuhealth.healthprofessional')
        User = Pool().get('res.user')
        user = User(Transaction().user)
        context = super(AppointmentDiaryReport, cls).get_context(records, data) #es un diccionario

        if data:
            start = data['start']
            end = data['end']
            final = data['end'] + timedelta(days=1)
            healthprof = data['healthprof']
            appointment = Appointments.search([
                ('appointment_date','>=',start),
                ('appointment_date','<',final),
                ('healthprof.id','=',healthprof),
                ('healthprof.institution.name.name','=',user.company.rec_name)
                ])

            healthprofessional = Healthprofessional.search([('id','=',healthprof)])

            context['healthprof'] = healthprofessional[0].name.rec_name
            context['specialty'] =\
                healthprofessional[0].main_specialty and healthprofessional[0].main_specialty.specialty.name or\
                healthprofessional[0].specialties and healthprofessional[0].specialties[0].specialty.name
            context['current_date'] = date.today()

            context['data'] = []

            if healthprofessional[0].main_specialty and healthprofessional[0].main_specialty.specialty.name == 'Odontología':
                context['odontology'] = True
            else:
               context['odontology'] = False

            for x in appointment:
                aux = []
                if context['odontology'] == True:
                    aux.append(x.patient.puid)
                    aux.append(x.patient.name.rec_name)
                    context['data'].append(aux)
                else:
                    aux.append(x.patient.puid)
                    aux.append(x.patient.name.rec_name)
                    aux.append(x.patient.age_float)
                    aux.append(x.patient.gender)
                    adress = ''
                    if x.patient.name.addresses[0].street is not None:
                        adress = x.patient.name.addresses[0].street
                    if x.patient.name.addresses[0].city is not None:
                        adress+= ", " + x.patient.name.addresses[0].city
                    if x.patient.name.addresses[0].subdivision is not None:
                        adress+= ", " + x.patient.name.addresses[0].subdivision.name
                    aux.append(adress)
                    current_insurance = ''
                    if x.patient.current_insurance is not None:
                        current_insurance = x.patient.current_insurance.company.name
                    aux.append(current_insurance)
                    context['data'].append(aux)

        return context






